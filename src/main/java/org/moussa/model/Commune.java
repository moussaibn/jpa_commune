package org.moussa.model;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries({
        @NamedQuery(
                name = "Commune.countPopulationUnder",
                query = "select sum (c.population) from Commune c where c.population < :taille "
        ),
        @NamedQuery(
                name = "Commune.populationRatioUnderAvg",
                query = "select " +
                        "sum(" +
                            "case when c.population<(select avg(c.population)from c )" +
                                "then c.population " +
                                "else 0 " +
                            "end)*100.0/sum(c.population) " +
                        "from Commune c"
        )
})
@Entity(name = "Commune")
public class Commune implements Serializable {

    @Id
    @Column(length = 5)
    private String id;

    @Column(length = 50)
    private String name;

    @OneToOne(cascade = CascadeType.PERSIST)
    private Maire maire;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Departement departement;

    private int population;

    public Commune() {
    }

    public Commune(String id, String name, int population) {
        this.id = id;
        this.name = name;
        this.population = population;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Maire getMaire() {
        return maire;
    }

    public void setMaire(Maire maire) {
        this.maire = maire;
    }

    public Departement getDepartement() {
        return departement;
    }

    public void setDepartement(Departement departement) {
        this.departement = departement;
    }

    public int getPopulation() {
        return population;
    }

    public void setPopulation(int population) {
        this.population = population;
    }

    public String getCodeDepartement() {
        return id.substring(0, 2);
    }

    @Override
    public String toString() {
        return "Commune{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", maire=" + maire +
                ", departement=" + departement +
                ", population=" + population +
                '}';
    }
}
