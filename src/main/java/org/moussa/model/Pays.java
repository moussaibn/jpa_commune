package org.moussa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

@Entity
public class Pays implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private int id;

    @Column(length = 50)
    private String nom;

    @OneToMany(mappedBy = "pays")
    @MapKey(name = "id")
    private Map<String, Departement> departements = new HashMap<>();

    public Pays() {
    }

    public Pays(String nom) {
        this.nom = nom;
    }

    public void addDepartement(Departement departement){
        this.departements.put(departement.getId(), departement);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public Set<Departement> getDepartements() {
        return new HashSet<>(departements.values());
    }

    public Departement getDepartementByCodePostal(String codePostal){
        return departements.get(codePostal);
    }
}
