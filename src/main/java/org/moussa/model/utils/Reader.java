package org.moussa.model.utils;

import org.moussa.model.Civility;
import org.moussa.model.Commune;
import org.moussa.model.Departement;
import org.moussa.model.Maire;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

public class Reader {
    private static String filename = "data-tp/maires-25-04-2014.csv";


    public static Map<String, Maire> readMaires() {
        Map<String, Maire> maires = new HashMap<>();
        Path path = Path.of(filename);
        try (BufferedReader br = Files.newBufferedReader(path)) {
            String line = br.readLine();
            while ((line = br.readLine()) != null) {

                String[] split = line.split(";");

                String last_name = split[5];
                String first_name = split[6];
                Civility civility = Civility.of(split[7]);
                DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
                Date date_of_birth = dateFormat.parse(split[8]);

                Maire maire = new Maire(first_name, last_name, civility, date_of_birth);

                String codePostale = CodePostaleFromLine(split);

                maires.put(codePostale, maire);
            }
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return maires;
    }

    public static Map<String, Commune> readCommunes() {
        Function<String, Commune> lineToCommune = x -> {
            String[] split = x.split(";");
            String name = split[3];
            int population = Integer.parseInt(split[4].trim());
            String codePostale = CodePostaleFromLine(split);
            return new Commune(codePostale, name,population);
        };
        Map<String, Commune> communes = new HashMap<>();
        Path path = Path.of(filename);
        try (BufferedReader br = Files.newBufferedReader(path)) {
            communes = br.lines().filter(l -> !l.startsWith("Code"))
                    .map(lineToCommune)
                    .collect(Collectors.toMap(Commune::getId, Function.identity(), (commune1, commune2) -> commune2));

        } catch (IOException e) {
            e.printStackTrace();
        }
        return communes;
    }

    public static Map<String, Departement> readDepartement(){
        Map<String, Departement> departements = new HashMap<>();
        Path path = Path.of(filename);
        try (BufferedReader br = Files.newBufferedReader(path)) {
            String line = br.readLine();
            while ((line = br.readLine()) != null) {

                String[] split = line.split(";");
                String codeDept = split[0].trim();
                if (codeDept.length() == 1) {
                    codeDept = "0" + codeDept;
                }
                String name = split[1];
                Departement departement = new Departement(codeDept, name);
                departements.put(codeDept, departement);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return departements;
    }

    private static String CodePostaleFromLine(String[] split) {
        String codeDept = split[0];
        String codeIns = split[2];
        if (codeDept.length() == 1) {
            codeDept = "0" + codeDept;
        }
        if (codeIns.length() == 1) {
            codeIns = "00" + codeIns;
        } else if (codeIns.length() == 2) {
            codeIns = "0" + codeIns;
        }
        return (codeDept + codeIns);
    }
}


