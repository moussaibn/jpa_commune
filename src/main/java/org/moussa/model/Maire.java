package org.moussa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Maire implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(length = 50)
    private String firstName;

    @Column(length = 50)
    private String lastName;

    @Column(length = 5)
    @Enumerated(EnumType.STRING)
    private Civility civility;

    @Temporal(TemporalType.DATE)
    private Date dateOfBirth;

    @OneToOne(mappedBy = "maire")
    private Commune commune;

    public Maire() {
    }

    public Maire(String firstName, String lastName, Civility civility, Date dateOfBirth) {
        this.lastName = lastName;
        this.firstName = firstName;
        this.civility = civility;
        this.dateOfBirth = new Date(dateOfBirth.getTime());
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Civility getCivility() {
        return civility;
    }

    public void setCivility(Civility civility) {
        this.civility = civility;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    @Override
    public String toString() {
        return "Maire{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", civility=" + civility +
                ", dateOfBirth=" + dateOfBirth +
                '}';
    }
}
