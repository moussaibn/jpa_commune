package org.moussa.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.*;

@NamedQueries({
        @NamedQuery(
                name = "Departement.getByName",
                query = "select departement from Departement departement where departement.name= :name "
        ),
        @NamedQuery(
                name = "Departement.getNumberOfCities",
                query = "select count(c) from Departement d, in(d.communes) c "
        ),
        @NamedQuery(
                name = "Departement.getOldestMayor",
                query = "select c.maire from Departement d, in(d.communes) c where d.name= :name order by c.maire.dateOfBirth asc"
        ),
        @NamedQuery(
                name = "Departement.countWomanMayorRatio",
                query = "select sum(case when c.maire.civility = 'M' then 0 else 1 end)*100.0/count(*) from Departement d, in(d.communes) c where d.name= :name"
        ),
        @NamedQuery(
                name = "Departement.statistics",
                query = "select new org.moussa.query.Statistics(count(c),sum(c.population),avg(c.population))from Departement d, in(d.communes) c where d.name= :name "
        )
})

@Entity(name = "Departement")
public class Departement implements Serializable {

    @Id
    @Column(length = 2)
    private String id;

    @Column(length = 50)
    private String name;

    @OneToMany(mappedBy = "departement")
    private Set<Commune> communes;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.PERSIST)
    private Pays pays;

    public Departement() {
    }

    public Departement(String id, String name) {
        this.id = id;
        this.name = name;
        communes = new HashSet<>();
    }

    public void addCommune(Commune commune) {
        commune.setDepartement(this);
        this.communes.add(commune);

    }

    public Set<Commune> getCommunes() {
        return new HashSet<>(this.communes);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getId() {
        return id;
    }

    public Pays getPays() {
        return pays;
    }

    public void setPays(Pays pays) {
        this.pays = pays;
    }

    @Override
    public String toString() {
        return "Departement{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}

