package org.moussa.query;

public class Statistics {
    private long CountCities;
    private long countPeople;
    private double average;

    public Statistics(long countCities, long countPeople, double average) {
        CountCities = countCities;
        this.countPeople = countPeople;
        this.average = average;
    }

    public long getCountCities() {
        return CountCities;
    }

    public void setCountCities(long countCities) {
        CountCities = countCities;
    }

    public long getCountPeople() {
        return countPeople;
    }

    public void setCountPeople(long countPeople) {
        this.countPeople = countPeople;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    @Override
    public String toString() {
        return "Statistics{" +
                "CountCities=" + CountCities +
                ", countPeople=" + countPeople +
                ", average=" + average +
                '}';
    }
}
