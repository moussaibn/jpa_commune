package org.moussa.exercice5;

import org.moussa.model.Commune;
import org.moussa.model.Departement;
import org.moussa.model.Maire;
import org.moussa.model.Pays;
import org.moussa.model.utils.Reader;

import javax.persistence.EntityManager;
        import javax.persistence.EntityManagerFactory;
        import javax.persistence.EntityTransaction;
        import javax.persistence.Persistence;
import java.util.Map;

public class LoadData {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-jpa-serie3");
        System.out.println("emf = " + emf);
        Map<String,Maire> maires = Reader.readMaires();
        Map<String, Commune> communes = Reader.readCommunes();
        Map<String, Departement> departements = Reader.readDepartement();
        for (Commune commune: communes.values()) {
            Maire maire =maires.get(commune.getId());
            commune.setMaire(maire);
            Departement departement = departements.get(commune.getCodeDepartement());
            departement.addCommune(commune);
        }
        Pays france = new Pays("France");
        departements.values().forEach(france::addDepartement);
        departements.values().forEach(departement-> departement.setPays(france));

        EntityManager entityManager = emf.createEntityManager();
        EntityTransaction transaction = entityManager.getTransaction();
        transaction.begin();
        communes.values().forEach(entityManager::persist);
        departements.values().forEach(entityManager::persist);
        entityManager.persist(france);
        transaction.commit();
    }
}
