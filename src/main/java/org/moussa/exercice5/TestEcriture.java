package org.moussa.exercice5;

import org.moussa.model.Commune;
import org.moussa.model.Departement;
import org.moussa.model.Maire;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.Set;

public class TestEcriture {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-jpa-serie3-validate");
        System.out.println("emf = " + emf);
        EntityManager entityManager = emf.createEntityManager();

        Commune commune = entityManager.find(Commune.class,"02653");
        Maire maire = commune.getMaire();
        Departement departement = commune.getDepartement();
        Set<Commune> communes = departement.getCommunes();

        System.out.println("La commune = " + commune.toString());

    }
}
