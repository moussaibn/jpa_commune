package org.moussa.exercice6;

import org.moussa.model.Departement;
import org.moussa.model.Maire;
import org.moussa.query.Statistics;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

public class Requetes {
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("tp-jpa-serie4");
        System.out.println("Connexion etablie avec la base");
        EntityManager entityManager = emf.createEntityManager();

        //Question 1:
        Query getDepartementByName = entityManager.createNamedQuery("Departement.getByName");
        getDepartementByName.setParameter("name", "Paris");
        Departement paris = (Departement) getDepartementByName.getSingleResult();
        System.out.println("Q1- Le departement ayant pour nom 'Paris' est " + paris);

        //Question 2:
        Query getNumberOfCities = entityManager.createNamedQuery("Departement.getNumberOfCities");
        //getNumberOfCities.setParameter("name", "AIN");
        long numberOfCities = (long) getNumberOfCities.getSingleResult();
        System.out.println("Q2- le nombre de communes est " + numberOfCities);

        //Question 3:
        Query getOldestMayor = entityManager.createNamedQuery("Departement.getOldestMayor");
        getOldestMayor.setParameter("name", "AIN");
        Maire oldestMayor = (Maire) getOldestMayor.getResultList().get(0);
        System.out.println("Q3- Le maire le plus agé dans le departement de Ain est " + oldestMayor);

        //Question 4:
        Query getWomanMayorRatio = entityManager.createNamedQuery("Departement.countWomanMayorRatio");
        getWomanMayorRatio.setParameter("name", "AIN");
        Double womanMayorRatio = (Double) getWomanMayorRatio.getSingleResult();
        System.out.println("Q4 - Le taux de femmes maires dans le departement Ain est " + womanMayorRatio);

        //Question 5:
        Query getStatiscis = entityManager.createNamedQuery("Departement.statistics");
        getStatiscis.setParameter("name", "PARIS");
        Statistics statistics = (Statistics) getStatiscis.getSingleResult();
        System.out.println("Q5- Les stats du departement de Paris " + statistics);

        //Question 6:
        Query getPopulationUnder = entityManager.createNamedQuery("Commune.countPopulationUnder");
        getPopulationUnder.setParameter("taille", 100000);
        long populationUnder = (long) getPopulationUnder.getSingleResult();
        System.out.println("Q6- La population vivant dans des communes de taille < à 100.000 habitants est " + populationUnder);

        //Question 7:
        Query getPopulationRatioUnderAvg = entityManager.createNamedQuery("Commune.populationRatioUnderAvg");
        Double populationRatioUnderAvg = (Double) getPopulationRatioUnderAvg.getSingleResult();
        System.out.println("Q7- le pourcentage de la population qui vit dans des communes de taille \n" +
                "plus petite que la taille moyenne d’une commune. est " + populationRatioUnderAvg);
    }
}
