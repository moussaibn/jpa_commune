# Serie 4
 
## Exercice 5:
> Question 2: **Quel est le type de la relation entre Commune et Maire ?** 

Un maire est rattaché à une seule commune et une Commune n'a qu'un seul maire. On a donc une relation 1:1 bidirectionnelle entre les deux classes. Elle(la relation) est bidirectionnelle car on peut retrouver le maire au travers de sa commune et on peut aussi retrouver la commune d'un maire

**Comment mapper cette relation ?**

On mappe la relation avec l'anotation ``OneToOne`` en ajoutant l'attribut ``mappedBy`` au niveau du maire assurer la bidirectionnalité de la relation au niceau de la classe Maire

> Question 3: **La relation entre Commune et Departement**
 
Une commune appartient à un unique Departement mais un departement à plusieurs commune.
On a donc une relation 1:n bidirectionnelle entre les deux classes. 
Pour enregistrer les communes d'un departement on utilise la structure ``Set`` qui va nous garantir qu'il n'y aura pas de doublon
 
> Question 4: **Quelle est le type de relation entre Pays et Departement ?**

Un depatement appartient à un unique pays mais un pays comporte plusieurs departements.
On a donc une relation 1:n entre les deux classes. 
Pour representer cette relation, sachant qu'on doit pouvoir  reccuperer un departement en connaissant uniquement son code postal, on utilisera la structure ``Map`` pour representer la collection de departement dans un pays. On mettra en clé le code postal du departement et en valeur le departement.

## Exercice 6: 

```
Q1- Le departement ayant pour nom 'Paris' est Departement{id='75', name='PARIS'}

Q2- le nombre de communes est 35884

Q3- Le maire le plus agé dans le departement de Ain est Maire{id=13359, firstName='Marcel', lastName='CHEVE', civility=M, dateOfBirth=1938-03-05}

Q4 - Le taux de femmes maires dans le departement Ain est 16.94511

Q5- Les stats du departement de Paris Statistics{CountCities=1, countPeople=2249975, average=2249975.0}

Q6- La population vivant dans des communes de taille < à 100.000 habitants est 54471869

Q7- le pourcentage de la population qui vit dans des communes de taille 
plus petite que la taille moyenne d’une commune. est 22.56328

Process finished with exit code 0
```